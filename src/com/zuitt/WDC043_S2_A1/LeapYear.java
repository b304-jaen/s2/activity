package com.zuitt.WDC043_S2_A1;
import java.util.Scanner;
public class LeapYear {

    public static void main(String[] args){

        System.out.println("Input a year to be checked if a leap year: ");
        Scanner yearScanner = new Scanner(System.in);
        int year = yearScanner.nextInt();

        if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
            System.out.println(year + " is a leap year");
        }else{
            System.out.println(year + " is not a leap year");
        }

    }


}
