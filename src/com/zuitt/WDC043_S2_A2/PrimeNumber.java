package com.zuitt.WDC043_S2_A2;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
public class PrimeNumber {
    public static void main(String[] args){

        int[] primeArray = {2, 3, 5, 7, 11};

        System.out.println("The first prime number is: " + primeArray[0]);
        System.out.println("The second prime number is: " + primeArray[1]);
        System.out.println("The third prime number is: " + primeArray[2]);
        System.out.println("The fourth prime number is: " + primeArray[3]);
        System.out.println("The fifth prime number is: " + primeArray[4]);

        ArrayList<String> friendsArray = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friendsArray);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + inventory);






    }



}
